<?php

return array(
    'modules' => array(
        'NetglueDefaults',
    ),

    'module_listener_options' => array(
        /**
         * Where to look for Module Configuration
         */
        'config_glob_paths'    => array(
            __DIR__.'/{,*.}{global,local}.php',
        ),

        /**
         * Where to look for modules
         */
        'module_paths' => array(
            __DIR__.'/../../vendor',
            __DIR__.'/../../',
        ),
    ),
);
