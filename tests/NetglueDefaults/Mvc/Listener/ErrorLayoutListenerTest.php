<?php

namespace NetglueDefaults\Mvc\Listener;

use Zend\Mvc\View\Http\ViewManager;


class ErrorLayoutListenerTest extends \Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase
{

    public function setUp()
    {
        $this->setApplicationConfig(
            include __DIR__.'/../../../config/application.config.php'
        );
    }

    public function testErrorLayoutIsSetFor404()
    {
        $this->dispatch('/not-found.'.uniqid());
        $this->assertResponseStatusCode(404);
        $this->assertTemplateName('layout/error');
    }

    public function testErrorLayoutIsNotSetInConsoleModels()
    {
        $app = $this->getApplication();
        $event = $app->getMvcEvent();
        $model = new \Zend\View\Model\ConsoleModel;
        $event->setViewModel($model);
        $this->assertInstanceOf('Zend\View\Model\ConsoleModel', $event->getViewModel(), 'Expected current app event to be a console view model');
        $listener = new ErrorLayoutListener('layout/error');
        $this->assertEmpty($model->getTemplate());
        $listener->onError($event);
        $this->assertEmpty($model->getTemplate());
    }

}
