<?php

namespace NetglueDefaults\Mvc\Controller;

use Zend\Mvc\View\Http\ViewManager;


class DefaultsControllerTest extends \Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase
{

    public function setUp()
    {
        $this->setApplicationConfig(
            include __DIR__.'/../../../config/application.config.php'
        );
    }

    public function testXmlViewStrategyIsAvailable()
    {
        $serviceLocator = $this->getApplicationServiceLocator();
        $class = 'NetglueDefaults\View\Strategy\XmlStrategy';
        $strategy = $serviceLocator->get($class);
        $this->assertInstanceOf($class, $strategy);
    }

    public function testTextViewStrategyIsAvailable()
    {
        $serviceLocator = $this->getApplicationServiceLocator();
        $class = 'NetglueDefaults\View\Strategy\TextStrategy';
        $strategy = $serviceLocator->get($class);
        $this->assertInstanceOf($class, $strategy);
    }

    public function testRobotsAction()
    {
        $this->dispatch('/robots.txt');
        $this->assertResponseStatusCode(200);

        $this->assertModuleName('NetglueDefaults');
        $this->assertMatchedRouteName('robotstxt');

        $this->assertResponseHeaderContains('content-type', 'text/plain; charset=utf-8');

        $this->assertTemplateName('netglue-defaults/defaults/robots');
        $viewModel = $this->getApplication()->getMvcEvent()->getViewModel();
        $this->assertInstanceOf('NetglueDefaults\View\Model\TextModel', $viewModel);

    }

    public function testSitemapXmlAction()
    {
        $this->dispatch('/sitemap.xml');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('NetglueDefaults');
        $this->assertMatchedRouteName('sitemapxml');
        $this->assertResponseHeaderContains('content-type', 'application/xml; charset=utf-8');
        $this->assertTemplateName('netglue-defaults/defaults/sitemap-xml');
        $viewModel = $this->getApplication()->getMvcEvent()->getViewModel();
        $this->assertInstanceOf('NetglueDefaults\View\Model\XmlModel', $viewModel);
    }

    public function testSitemapAction()
    {
        $this->dispatch('/sitemap');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('NetglueDefaults');
        $this->assertMatchedRouteName('sitemap');
        $this->assertTemplateName('netglue-defaults/defaults/sitemap');
        $viewModel = $this->getApplication()->getMvcEvent()->getViewModel();
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $viewModel);
    }

    public function testGoogleVerifyActionEmits404ForInvalidCode()
    {
        $this->dispatch('/googlefoo.html');
        $this->assertResponseStatusCode(404);
    }

    public function testGoogleVerifyActionReturns200WhenCodeIsSet()
    {
        // Set the expected verify code
        $path = 'googleabcdefadd.html';
        $serviceLocator = $this->getApplicationServiceLocator();
        $ctrlManager = $serviceLocator->get('ControllerManager');
        $controller = $ctrlManager->get('NetglueDefaults\Mvc\Controller\DefaultsController');
        $controller->setGoogleVerificationFile($path);
        $this->assertSame($path, $controller->getGoogleVerificationFile());

        // Dispatch to the expected url and make sure we have a 200
        $this->dispatch('/'.$path);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('NetglueDefaults');
        $this->assertMatchedRouteName('googleverify');
        $this->assertTemplateName('netglue-defaults/defaults/google-verify');
        $viewModel = $this->getApplication()->getMvcEvent()->getViewModel();
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $viewModel);
    }

}
