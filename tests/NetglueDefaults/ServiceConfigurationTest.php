<?php

namespace NetglueDefaults;


use Zend\ServiceManager\ServiceLocatorInterface;

class ServiceConfigurationTest extends \PHPUnit_Framework_TestCase
{

    public function getServiceLocator()
    {
        return TestBootstrap::getServiceManager();
    }

    public function testServiceLocatorIsAvailable()
    {
        $sl = $this->getServiceLocator();
        $this->assertInstanceOf('Zend\ServiceManager\ServiceManager', $sl);

        return $sl;
    }



}
