<?php

namespace NetglueDefaults\View\Model;

class TextModelTest extends \PHPUnit_Framework_TestCase
{

    public function testCanInstantiate()
    {
        $model = new TextModel;
        $this->assertInstanceOf('NetglueDefaults\View\Model\TextModel', $model);
        return $model;
    }

    /**
     * @depends testCanInstantiate
     */
    public function testSetGetEncoding(TextModel $model)
    {
        $this->assertEquals('utf-8', $model->getEncoding());
        $model->setEncoding('Foo');
        $this->assertSame('Foo', $model->getEncoding());
    }

    /**
     * @depends testCanInstantiate
     */
    public function testSetGetContentType(TextModel $model)
    {
        $this->assertEquals('text/plain', $model->getContentType());
        $model->setContentType('other/type');
        $this->assertSame('other/type', $model->getContentType());
    }

}
