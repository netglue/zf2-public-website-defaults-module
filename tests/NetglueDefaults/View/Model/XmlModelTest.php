<?php

namespace NetglueDefaults\View\Model;

class XmlModelTest extends \PHPUnit_Framework_TestCase
{

    public function testCanInstantiate()
    {
        $model = new XmlModel;
        $this->assertInstanceOf('NetglueDefaults\View\Model\XmlModel', $model);
        return $model;
    }

    /**
     * @depends testCanInstantiate
     */
    public function testSetGetEncoding(XmlModel $model)
    {
        $this->assertEquals('utf-8', $model->getEncoding());
        $model->setEncoding('Foo');
        $this->assertSame('Foo', $model->getEncoding());
    }

    /**
     * @depends testCanInstantiate
     */
    public function testSetGetContentType(XmlModel $model)
    {
        $this->assertEquals('application/xml', $model->getContentType());
        $model->setContentType('other/type');
        $this->assertSame('other/type', $model->getContentType());
    }

}
