<?php
/**
 * Defaults Module Controller
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 */

namespace NetglueDefaults\Mvc\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use NetglueDefaults\View\Model\XmlModel;
use NetglueDefaults\View\Model\TextModel;

use Zend\Mvc\Application;
use Zend\Http\Response as HttpResponse;
use Zend\Mvc\MvcEvent;

/**
 * Defaults Module Controller
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 */
class DefaultsController extends AbstractActionController
{

    /**
     * Google Verification File Name
     * @var string
     */
    protected $googleVerify;

    /**
     * XML Sitemap
     * @return XmlModel
     */
    public function sitemapXmlAction()
    {
        return new XmlModel;
    }

    /**
     * Normal Sitemap
     * @return ViewModel
     */
    public function sitemapAction()
    {
        return new ViewModel;
    }

    /**
     * Robots text action
     * @return TextModel
     */
    public function robotsAction()
    {
        return new TextModel;
    }

    /**
     * Verification File for Google
     * @return ViewModel
     */
    public function googleVerifyAction()
    {
        $requestPath = trim($this->getRequest()->getUri()->getPath(), '/');
        if(null === $this->googleVerify || $requestPath !== $this->googleVerify) {
            $e = $this->getEvent();
			$e->setError(Application::ERROR_CONTROLLER_CANNOT_DISPATCH);
			$response = $e->getResponse();
			if($response instanceof HttpResponse) {
				$response->setStatusCode(404);
			}
			$this->getEventManager()->trigger(MvcEvent::EVENT_DISPATCH_ERROR, $this->getEvent());
			return;
        }

        $this->layout()->setTemplate('layout/blank');

        return new ViewModel;
    }

    /**
     * Set the google verification path
     * @param string $fileName
     * @return self
     */
    public function setGoogleVerificationFile($fileName)
    {
        $fileName = empty($fileName) || !is_string($fileName) ? NULL : $fileName;
        $this->googleVerify = trim($fileName, '/');

        return $this;
    }

    /**
     * Get the google verification path
     * @return string|NULL
     */
    public function getGoogleVerificationFile()
    {
        return $this->googleVerify;
    }

}
