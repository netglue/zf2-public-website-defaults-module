<?php

/**
 * If the module is configured such that this listener is attached on bootstrap,
 * it will listen for MVC error events and set the view layout template to something else (Configured)
 *
 * The ViewModel must be a regular ViewModel, i.e. Console/Json/Other will be silently ignored
 */

namespace NetglueDefaults\Mvc\Listener;

use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;

use Zend\Mvc\Exception;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ModelInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Model\ConsoleModel;
use Zend\Http\Header\ContentType;

class ErrorLayoutListener extends AbstractListenerAggregate
{
    /**
     * Layout to set in the View Model
     * @var string
     */
    private $layout;

    /**
     * Whether we should switch layout or not
     * @var bool
     */
    private $shouldSwitch = true;

    /**
     * Whether we should only set layouts when the content type header is set
     * @var bool
     */
    private $requireEmptyContentType = true;

    /**
     * Constructor requires a layout string
     * @param string $layout
     * @return void
     */
    public function __construct($layout)
    {
        $this->setLayoutTemplate($layout);
    }

    /**
     * Set Layout
     * @param string $layout
     * @return self
     */
    public function setLayoutTemplate($layout)
    {
        $this->layout = (string) $layout;

        return $this;
    }

    /**
     * Return configured layout name
     * @return string
     */
    public function getLayoutTemplate()
    {
        return $this->layout;
    }

    /**
     * Return the view model set in the MvcEvent
     * @param MvcEvent $event
     * @return ModelInterface|null
     */
    private function getViewModel(MvcEvent $event)
    {
        $viewModel = $event->getViewModel();

        /**
         * Don't test the interface test for an actual ViewModel or decendent class
         */
        if (!$viewModel instanceof ViewModel) {
            return null;
        }
        // The ViewModel must not be a console model
        if($viewModel instanceof ConsoleModel) {
            return null;
        }
        return $viewModel;
    }

    /**
     * Set the configured template in the view model found in the event
     *
     * Only sets the layout if a view model is found
     *
     * @param MvcEvent $event
     * @return self
     */
    private function setViewLayout(MvcEvent $event)
    {
        $viewModel = $this->getViewModel($event);
        if ($viewModel instanceof ModelInterface) {
            $viewModel->setTemplate($this->getLayoutTemplate());
        }
        return $this;
    }

    /**
     * Attach to the given event manager
     * @param EventManagerInterface $events
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $events->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onError'));
        $events->attach(MvcEvent::EVENT_RENDER_ERROR, array($this, 'onError'));
    }

    /**
     * Error Event Callback
     * @param MvcEvent $event
     * @return void
     */
    public function onError(MvcEvent $event)
    {
        if($this->shouldSetLayout($event)) {
            $this->setViewLayout($event);
        }
    }

    /**
     * Whether we should switch layout based on parameter contained in the given event
     * @param MvcEvent $event
     * @return bool
     */
    public function shouldSetLayout(MvcEvent $event)
    {
        // If the view model is not accessible...
        if(!$this->getViewModel($event)) {
            return false;
        }
        // If the content-type header is set to anything other than blank...
        if($this->requireEmptyContentType && $this->contentTypeHeaderSet($event)) {
            return false;
        }

        return $this->shouldSwitch;
    }

    /**
     * Whether the content type header is set in the response found in the MvcEvent
     * @param MvcEvent $event
     * @return bool
     */
    public function contentTypeHeaderSet(MvcEvent $event)
    {
        $mediaType = $this->getContentTypeHeaderFromEvent($event);
        return false !== $mediaType;
    }

    /**
     * Return the value of the content-type header or false
     * @param MvcEvent $event
     * @return string|false
     */
    public function getContentTypeHeaderFromEvent(MvcEvent $event)
    {
        if( ! $response = $event->getResponse() ) {
            return false;
        }
        $headers = $response->getHeaders();
        $header = $headers->get('content-type');
        if($header instanceof ContentType) {
            return $header->getMediaType();
        }
        return false;
    }

    /**
     * Setter to disable/enable setting the layout on error conditions
     * @param bool $flag
     * @return self
     */
    public function setShouldSwitchLayout($flag)
    {
        $this->shouldSwitch = (bool) $flag;

        return $this;
    }

}
