<?php
/**
 * Factory for creating the 'Defaults' Controller
 */

namespace NetglueDefaults\Mvc\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use NetglueDefaults\Mvc\Controller\DefaultsController;

class DefaultsControllerFactory implements FactoryInterface
{
    /**
     * Create and return the Defaults Controller
     *
     * @param  ServiceLocatorInterface $serviceLocator
     * @return DefaultsController
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $controller = new DefaultsController;

        $config = $serviceLocator->getServiceLocator()->get('Config');
        if(isset($config['site_data']['google_verify_code'])) {
            $controller->setGoogleVerificationFile($config['site_data']['google_verify_code']);
        }

        return $controller;
    }

}
