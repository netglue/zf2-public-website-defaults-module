<?php
/**
 * Factory for creating XML Rendering Strategy
 */

namespace NetglueDefaults\Mvc\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use NetglueDefaults\View\Strategy\XmlStrategy;

class ViewXmlStrategyFactory implements FactoryInterface
{
    /**
     * Create and return the XML view strategy
     *
     * Retrieves the PhpRenderer from the service locator, and
     * injects it into the constructor for the XML strategy.
     *
     * @param  ServiceLocatorInterface $serviceLocator
     * @return XmlStrategy
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $phpRenderer = $serviceLocator->get('Zend\View\Renderer\PhpRenderer');
        $xmlStrategy = new XmlStrategy($phpRenderer);

        return $xmlStrategy;
    }

}
