<?php
/**
 * Factory for creating Text Rendering Strategy
 */

namespace NetglueDefaults\Mvc\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use NetglueDefaults\View\Strategy\TextStrategy;

class ViewTextStrategyFactory implements FactoryInterface
{
    /**
     * Create and return the Text view strategy
     *
     * Retrieves the PhpRenderer from the service locator, and
     * injects it into the constructor for the Text strategy.
     *
     *
     * @param  ServiceLocatorInterface $serviceLocator
     * @return TextStrategy
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $phpRenderer = $serviceLocator->get('Zend\View\Renderer\PhpRenderer');
        $txtStrategy = new TextStrategy($phpRenderer);

        return $txtStrategy;
    }

}
