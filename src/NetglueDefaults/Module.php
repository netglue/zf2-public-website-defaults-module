<?php
/**
 * Netglue Defaults Module
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @link https://bitbucket.org/netglue/zf2-skeleton
 */

namespace NetglueDefaults;

/**
 * Config Provider
 */
use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Autoloader
 */
use Zend\Loader\AutoloaderFactory;
use Zend\Loader\StandardAutoloader;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;

/**
 * Controller Provider
 */
use Zend\ModuleManager\Feature\ControllerProviderInterface;

/**
 * Service Provider
 */
use Zend\ModuleManager\Feature\ServiceProviderInterface;

/**
 * View Helper Provider
 */
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;

/**
 * BootstrapListener
 */
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\EventManager\EventInterface;

/**
 * Mvc Event
 */
use Zend\Mvc\MvcEvent;

use NetglueDefaults\Mvc\Listener\ErrorLayoutListener;

/**
 * Netglue Defaults Module
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @link https://bitbucket.org/netglue/zf2-skeleton
 */
class Module implements
    AutoloaderProviderInterface,
    ControllerProviderInterface,
    ServiceProviderInterface,
    ConfigProviderInterface,
    ViewHelperProviderInterface,
    BootstrapListenerInterface
{

    /**
     * Bootstrap Listener
     * @param EventInterface $event
     * @return void
     */
    public function onBootstrap(EventInterface $event)
    {
        // Instanceof \Zend\Mvc\Application
        $application = $event->getApplication();

        // Instanceof \Zend\ServiceManager\ServiceManager
        $serviceLocator = $application->getServiceManager();

        $config = $serviceLocator->get('Config');
        $useErrorLayout = isset($config['view_manager']['use_error_layout']) && $config['view_manager']['use_error_layout'] === true;
        if($useErrorLayout) {
            $eventManager = $application->getEventManager();
            $listener = new ErrorLayoutListener($config['view_manager']['error_layout']);
            $listener->attach($eventManager);
        }
    }


    /**
     * Return Controller Config
     * @return array
     */
    public function getControllerConfig()
    {
        return array(
            'factories' => array(
                'NetglueDefaults\Mvc\Controller\DefaultsController' => 'NetglueDefaults\Mvc\Service\DefaultsControllerFactory'
            ),
        );
    }

    /**
     * Return Service Config
     * @return array
     * @implements ServiceProviderInterface
     */
    public function getServiceConfig()
    {
        return include __DIR__ . '/../../config/services.config.php';
    }

    /**
     * Include/Return module configuration
     * @return array
     * @implements ConfigProviderInterface
     */
    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

    /**
     * Return autoloader configuration
     * @link http://framework.zend.com/manual/2.0/en/user-guide/modules.html
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            AutoloaderFactory::STANDARD_AUTOLOADER => array(
                StandardAutoloader::LOAD_NS => array(
                    __NAMESPACE__ => __DIR__,
                ),
            ),
        );
    }

    /**
	 * Return view helper plugin config
	 * @return array
	 * @implements ViewHelperProviderInterface
	 */
	public function getViewHelperConfig()
	{
		return array(
			'factories' => array(
				'NetglueDefaults\View\Helper\SiteData' => 'NetglueDefaults\Service\SiteDataViewHelperFactory',
			),
			'aliases' => array(
				'siteData' => 'NetglueDefaults\View\Helper\SiteData',
			),
		);
	}

}
