<?php
/**
 * Plain Text View Strategy
 */

namespace NetglueDefaults\View\Strategy;

use NetglueDefaults\View\Model\TextModel;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\View\ViewEvent;
use Zend\View\Renderer\PhpRenderer;
use Zend\Http\Header\ContentType;

class TextStrategy implements ListenerAggregateInterface
{
    /**
     * @var \Zend\Stdlib\CallbackHandler[]
     */
    protected $listeners = array();

    /**
     * @var PhpRenderer
     */
    protected $renderer;

    /**
     * Constructor
     *
     * @param PhpRenderer $renderer
     */
    public function __construct(PhpRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * Attach the aggregate to the specified event manager
     *
     * This always gets attached at priority 100
     * See: \Zend\Mvc\View\Http\ViewManager::registerViewStrategies()
     *
     * @param  EventManagerInterface $events
     * @param  int                   $priority
     * @return void
     */
    public function attach(EventManagerInterface $events, $priority = 100)
    {
        $this->listeners[] = $events->attach(ViewEvent::EVENT_RENDERER, array($this, 'selectRenderer'), $priority);
        $this->listeners[] = $events->attach(ViewEvent::EVENT_RESPONSE, array($this, 'injectResponse'), $priority);
    }

    /**
     * Detach aggregate listeners from the specified event manager
     *
     * @param  EventManagerInterface $events
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    /**
     * Detect if we should use the TextRenderer based on model type
     * This method could be extended to search header for appropriate accept values
     *
     * @param  ViewEvent         $e
     * @return null|PhpRenderer
     */
    public function selectRenderer(ViewEvent $e)
    {
        $model = $e->getModel();
        if (!$model instanceof TextModel) {
            // no TextModel; do nothing
            return;
        }
        // TextModel found
        return $this->renderer;
    }

    /**
     * Inject the response with the Plain Text payload and appropriate Content-Type header
     *
     * @param  ViewEvent $e
     * @return void
     */
    public function injectResponse(ViewEvent $e)
    {
        /**
         * Double check the model is one we recognise as we're re-using the PHP Renderer
         * So the renderer is often likely to be ours...
         */
        if(!$this->selectRenderer($e)) {
            return;
        }

        /**
         * Inspect the renderer inside the event - if it does not match our
         * renderer instance, it's not our job to return the response body
         */
        $renderer = $e->getRenderer();
        if ($renderer !== $this->renderer) {
            // Discovered renderer is not ours; do nothing
            return;
        }

        $result = $e->getResult();
        if (!is_string($result)) {
            // We don't have a string, and thus, no Text
            return;
        }

        /**
         * If an exception has been thrown during rendering,
         * Then the content of $result will be the Error output
         * from the standard mvc exception strategy.
         * So, we should probably only set content type headers
         * when the response is marked as successful.
         *
         * If you wanted to return plain-text errors, you'd likely
         * be doing something more advanced in the first place and therefore
         * not using this module at all.
         */

        $model = $e->getModel();
        // Populate response
        $response = $e->getResponse();
        $response->setContent($result);

        if(!$response->isSuccess()) {
            return;
        }

        $headers = $response->getHeaders();
        if(!$header = $headers->get('content-type')) {
            $header = new ContentType;
        }
        $header->setMediaType('text/plain');
        if ($model->getEncoding()) {
            $header->setCharset($model->getEncoding());
        }
        if ($model->getContentType()) {
            $header->setMediaType($model->getContentType());
        }

        $headers->addHeader($header);
    }

}
