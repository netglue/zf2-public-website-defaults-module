<?php
/**
 * Simple view helper redirects method calls to the abstract options class: Silver\Service\SiteData
 */

namespace NetglueDefaults\View\Helper;

use Zend\View\Helper\AbstractHelper;

use NetglueDefaults\Service\SiteData as SiteOptions;

class SiteData extends AbstractHelper
{
    /**
     * Site Data Service set in constructor
     * @var SiteOptions
     */
    protected $options;

    /**
     * Constructor requires site data service
     * @param  SiteOptions $options
     * @return void
     */
    public function __construct(SiteOptions $options)
    {
        $this->options = $options;
    }

    public function __invoke()
    {
        return $this;
    }

    /**
     * Redirect everything to the options object
     * @param  string $method
     * @param  array  $args
     * @return mixed
     */
    public function __call($method, $args = NULL)
    {
        return call_user_func_array(array($this->options, $method), $args);
    }

}
