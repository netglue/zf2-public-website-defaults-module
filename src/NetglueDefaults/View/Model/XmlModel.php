<?php
namespace NetglueDefaults\View\Model;

use Zend\View\Model\ViewModel;

class XmlModel extends ViewModel
{
    /**
     * XML probably won't need to be captured into a
     * a parent container by default.
     *
     * @var string
     */
    protected $captureTo = null;

    /**
     * XML is usually terminal
     *
     * @var bool
     */
    protected $terminate = true;

    /**
     * UTF-8 Default Encoding
     * @var string
     */
    protected $encoding = 'utf-8';

    /**
     * Content Type Header
     * @var string
     */
    protected $contentType = 'application/xml';

    /**
     * Set Encoding
     * @param  string   $encoding
     * @return XmlModel
     */
    public function setEncoding($encoding)
    {
        $this->encoding = $encoding;

        return $this;
    }

    /**
     * Get Encoding
     * @return string
     */
    public function getEncoding()
    {
        return $this->encoding;
    }

    /**
     * Set Content Type
     * @param  string   $contentType
     * @return XmlModel
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * Get Content Type
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }

}
