<?php

namespace NetglueDefaults\Service;

use NetglueDefaults\View\Helper\SiteData as Helper;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SiteDataViewHelperFactory implements FactoryInterface
{
    /**
     * Return a SiteData View Helper instance
     *
     * @param  ServiceLocatorInterface         $serviceLocator
     * @return SiteData
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $appServices = $serviceLocator->getServiceLocator();
        $options = $appServices->get('NetglueDefaults\Service\SiteData');
        $helper = new Helper($options);
        return $helper;
    }

}
