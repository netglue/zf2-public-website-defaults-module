<?php

namespace NetglueDefaults\Service;

use Zend\Stdlib\AbstractOptions;

use Zend\View\Helper\HeadMeta;

class SiteData extends AbstractOptions
{
    protected $data = array(
        'companyName' => '',
        'phoneNumber' => '',
        'addressArray' => array(),
        'coNumber' => '',
        'vatNumber' => '',
        'publicEmail' => '',
        'latitude' => NULL,
        'longitude' => NULL,
        'facebookAdmins' => array(),
        'openGraph' => array(
            'type' => 'website',
            'site_name' => NULL,
        ),
        'googleVerifyCode' => NULL,
    );

    protected $metaHelper;

    public function setMetaHelper(HeadMeta $helper)
    {
        $this->metaHelper = $helper;

        return $this;
    }

    public function getMetaHelper()
    {
        return $this->metaHelper;
    }

    public function setPhoneNumber($num)
    {
        $this->data['phoneNumber'] = $num;

        return $this;
    }

    public function getPhoneNumber()
    {
        return $this->data['phoneNumber'];
    }

    public function setAddressArray(array $array)
    {
        $this->data['addressArray'] = $array;

        return $this;
    }

    public function getAddressArray()
    {
        return isset($this->data['addressArray']) ? $this->data['addressArray'] : array();
    }

    public function getAddressAsString($separator = ', ')
    {
        $out = array();
        foreach ($this->getAddressArray() as $part) {
            if (!empty($part)) {
                $out[] = $part;
            }
        }

        return implode($separator, $out);
    }

    public function setCompanyName($name)
    {
        $this->data['companyName'] = $name;

        return $this;
    }

    public function getCompanyName()
    {
        return $this->data['companyName'];
    }

    public function setVatNumber($num)
    {
        $this->data['vatNumber'] = $num;

        return $this;
    }

    public function getVatNumber()
    {
        return $this->data['vatNumber'];
    }

    public function setCoNumber($num)
    {
        $this->data['coNumber'] = $num;

        return $this;
    }

    public function getCoNumber()
    {
        return $this->data['coNumber'];
    }

    public function setPublicEmail($email)
    {
        $this->data['publicEmail'] = $email;

        return $this;
    }

    public function getPublicEmail()
    {
        return $this->data['publicEmail'];
    }

    public function setLatitude($latitude)
    {
        $this->data['latitude'] = $latitude;

        return $this;
    }

    public function getLatitude()
    {
        return $this->data['latitude'];
    }

    public function setOpenGraph(array $data)
    {
        $this->data['openGraph'] = $data;

        return $this;
    }

    public function getOpenGraph()
    {
        return $this->data['openGraph'];
    }

    public function setOgSiteName($num)
    {
        $this->data['openGraph']['site_name'] = $num;

        return $this;
    }

    public function getOgSiteName()
    {
        $name = isset($this->data['openGraph']['site_name']) ? $this->data['openGraph']['site_name'] : NULL;

        return $name;
    }

    public function setLongitude($longitude)
    {
        $this->data['longitude'] = $longitude;

        return $this;
    }

    public function getLongitude()
    {
        return $this->data['longitude'];
    }

    public function setFacebookAdmins(array $admins)
    {
        $this->data['facebookAdmins'] = $admins;

        return $this;
    }

    public function getFacebookAdmins()
    {
        return $this->data['facebookAdmins'];
    }

    public function hasFacebookAdmins()
    {
        return count($this->data['facebookAdmins']) >= 1;
    }

    public function hasLocation()
    {
        return (!is_null($this->getLatitude()) && !is_null($this->getLongitude()));
    }

    public function setGoogleVerifyCode($code)
    {
        $this->data['googleVerifyCode'] = $code;

        return $this;
    }

    public function getGoogleVerifyCode()
    {
        return $this->data['googleVerifyCode'];
    }

    public function metaPropertiesToHtml5String($separator = "\n")
    {
        /**
         * We can't use meta properties and an HTML5 doctype with Zend's HeadMeta helper
         * as it considers them invalid, but the W3C validator says they're OK...
         */
        $format = '<meta property="%s" content="%s">';
        $props = array();

        if ($this->hasFacebookAdmins()) {
            $props[] = array('fb:admins', implode(',',$this->getFacebookAdmins()));
        }


        $meta = array();
        foreach ($props as $data) {
            $meta[] = sprintf($format, $data[0], $data[1]);
        }

        return implode($separator, $meta);
    }

    public function applyMetaData(HeadMeta $helper = null)
    {
        if (NULL !== $helper) {
            $this->setMetaHelper($helper);
        }
        if (!$this->metaHelper instanceof HeadMeta) {
            throw new \RuntimeException('Cannot apply meta data without an instance of HeadMeta');
        }
        $helper = $this->metaHelper;
        if ($this->hasFacebookAdmins()) {
            $admins = implode(',',$this->getFacebookAdmins());
        }

        return $this;
    }

}
