<?php

namespace NetglueDefaults\Service;


use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SiteDataFactory implements FactoryInterface
{
    /**
     * Return a SiteData instance
     *
     * @param  ServiceLocatorInterface         $serviceLocator
     * @return SiteData
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');
        $config = isset($config['site_data']) ? $config['site_data'] : array();
        $options = new SiteData($config);
        return $options;
    }

}
