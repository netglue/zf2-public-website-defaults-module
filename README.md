# Netglue Public Website Defaults Module

## Work In Progress
[![Latest Stable Version](https://poser.pugx.org/netglue/public-website-defaults-module/v/stable.svg)](https://packagist.org/packages/netglue/public-website-defaults-module) [![Total Downloads](https://poser.pugx.org/netglue/public-website-defaults-module/downloads.svg)](https://packagist.org/packages/netglue/public-website-defaults-module) [![Latest Unstable Version](https://poser.pugx.org/netglue/public-website-defaults-module/v/unstable.svg)](https://packagist.org/packages/netglue/public-website-defaults-module) [![License](https://poser.pugx.org/netglue/public-website-defaults-module/license.svg)](https://packagist.org/packages/netglue/public-website-defaults-module)

If you use this package, bear in mind it's developed mainly for our own use at [Netglue](http://netglue.co) so minor version bumps will very possibly have BC Breaks. Generally, patch versions will remain as stable as it was before.

## Introduction

This is a simple module to configure utility stuff that's used for the majority of my public websites.

Simply install with composer, add to your module list and you've got working routes for common files.

## View Rendering Strategies

Plain text and XML view rendering strategies are registed with the view manager so that you can easily template these types of content using the standard templating system.

From your controller, all you have to do to use them is return a `NetglueDefaults\View\Model\TextModel` or `XmlModel` and make sure that a template is available to render that can be found in your path stack or template map.

The xml and text view models are marked as terminal so layouts won't be rendered. The models also set the default `content-type` header appropriately and use UTF-8 by default.

## Defaults Controller with Initial Routes

There are a few routes that are useful for simple public sites.

* Google Verification at `/google[a-z0-9]+\.html`
* XML Sitemap from the default navigation container at `/sitemap.xml`
* HTML Site Map at `/sitemap`
* Robots Text at `/robots.txt`

The google verification endpoint uses the `SiteData` view helper which is a simple wrapper around `Zend\Stdlib\AbstractOptions` instance

## View Manager Boilerplate and Error views

I often don't start with a Skeleton app so having this already setup is helpful to me.

## Error Listener, Layout Switcher

By default a listener is configured to watch out for the events `MvcEvent::EVENT_DISPATCH_ERROR` and `MvcEvent::EVENT_RENDER_ERROR` and set the view layout template when an error occurs.

The following config options are relevant here:
    
    $config['view_manager']['use_error_layout'] = true;
    $config['view_manager']['error_layout'] = 'layout/error';

Whatever layout template is set needs to be resolvable in the template path stack or map.

## Install

Install with composer: `netglue/public-website-defaults-module` as a dependency in a ZF2 app.

Add to your list of modules with the name `NetglueDefaults`

Take a look at `config/module.config.php`

## Changelog

### 1.1.0

* Fixed a serious problem where the custom renderers would steal the view helper plugin manager, thereby making `$this->partial()` fail to find any of the expected templates. Solved this by throwing out the custom renderers and template resolvers.
* The Google verify controller action will appropriately throw a 404 when no verification code is configured, or the requested url doesn't match the configured file.
* Removed all reference to Nibbler verification as it's been discontinued.
* Added a default error listener that switches layout when there's one of `MvcEvent::EVENT_DISPATCH_ERROR` or `MvcEvent::EVENT_RENDER_ERROR` providing we have a regular view model as opposed to a console view model.

### 1.0.1

* Fixing my screw up with composer and repo tags

### 1.0.0

* Initial Release

