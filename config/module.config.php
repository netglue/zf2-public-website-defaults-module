<?php

/**
 * This is used to switch error template names.
 */
$env = defined('APPLICATION_ENVIRONMENT') ? APPLICATION_ENVIRONMENT : 'production';
$env = (!in_array($env, array('production', 'development'))) ? 'development' : $env;

return array(

    /**
     * Empty Default Navigation
     */
    'navigation' => array(
        'default' => array(

        ),
    ),

    /**
     * Useful View Helper
     * Use in view templates as $this->siteData()->getPhoneNumber();
     * Nothing the view helper outputs is escaped.
     */
    'site_data' => array(
        'google_verify_code' => NULL, // Use the full file name, i.e. 'google12345abdadd.html'
        'phone_number' => NULL,
        'co_number' => NULL,
        'company_name' => NULL,
        'vat_number' => NULL,
        'address_array' => array(),
        'public_email' => NULL,
        'latitude' => NULL,
        'longitude' => NULL,
        'open_graph' => array(

        ),
        'facebook_admins' => array(),
    ),

    /**
     * Specify some default common routes.
     * All of these routes point to the NetglueDefaults\Mvc\Controller\DefaultsController
     * which uses the registered XML and Plain Text rendering strategies
     */
    'router' => array(
        'routes' => array(
            'sitemapxml' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/sitemap.xml',
                    'defaults' => array(
                        'controller' => 'NetglueDefaults\Mvc\Controller\DefaultsController',
                        'action' => 'sitemap-xml',
                    ),
                ),
            ),
            'sitemap' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/sitemap',
                    'defaults' => array(
                        'controller' => 'NetglueDefaults\Mvc\Controller\DefaultsController',
                        'action' => 'sitemap',
                    ),
                ),
            ),
            'robotstxt' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/robots.txt',
                    'defaults' => array(
                        'controller' => 'NetglueDefaults\Mvc\Controller\DefaultsController',
                        'action' => 'robots',
                    ),
                ),
            ),
            'googleverify' => array(
                'type' => 'Regex',
                'options' => array(
                    'regex' => '/google([a-z0-9]+?)\.html',
                    'defaults' => array(
                        'controller' => 'NetglueDefaults\Mvc\Controller\DefaultsController',
                        'action' => 'google-verify',
                    ),
                    'spec' => '/google%id%add.html',
                ),
            ),
        ),
    ),

    /**
     * View Manager
     */
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/500',

        /**
         * Turn on/off the listener that switches layout when an error/exception occurs
         */
        'use_error_layout'         => true,
        'error_layout'             => 'layout/error',

        'template_map' => array(

            /**
             * It's expected that the layouts would be overriden
             * Bear in mind that google verification will fail if you overide
             * layout/blank with something that has content in it
             */
            'layout/blank'  => __DIR__ . '/../view/layout/blank.phtml',
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'layout/error'  => __DIR__ . '/../view/layout/error.phtml',

            /**
             * Default view templates for error pages
             */
            'error/404' => __DIR__ . '/../view/error/404-'.$env.'.phtml',
            'error/500' => __DIR__ . '/../view/error/500-'.$env.'.phtml',

            /**
             * Default templates for various utility urls
             */
            'netglue-defaults/defaults/google-verify' => __DIR__ . '/../view/defaults/google-verify.phtml',
            'netglue-defaults/defaults/sitemap' => __DIR__ . '/../view/defaults/sitemap.phtml',
            'netglue-defaults/defaults/sitemap-xml' => __DIR__ . '/../view/defaults/sitemap-xml.xml.php',
            'netglue-defaults/defaults/robots' => __DIR__ . '/../view/defaults/robots.txt.php',
        ),


        /**
         * Register View Strategies with the View Manager
         * Corresponding Factories can be found in service config
         */
        'strategies' => array(
            'NetglueDefaults\View\Strategy\TextStrategy',
            'NetglueDefaults\View\Strategy\XmlStrategy',
        ),
    ),
);
