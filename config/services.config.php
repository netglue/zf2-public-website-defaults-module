<?php
/**
 * Services Configuration
 */
return array(

    'factories' => array(

        /**
         * View Rendering Strategies
         * Registered with the view manager in module.config.php under [view_manager][strategies]
         */
        'NetglueDefaults\View\Strategy\TextStrategy' => 'NetglueDefaults\Mvc\Service\ViewTextStrategyFactory',
        'NetglueDefaults\View\Strategy\XmlStrategy'  => 'NetglueDefaults\Mvc\Service\ViewXmlStrategyFactory',

        /**
         * Navigation Setup
         */
        'Zend\Navigation\Service\DefaultNavigationFactory' => 'Zend\Navigation\Service\DefaultNavigationFactory',

        // Site Information Options
        'NetglueDefaults\Service\SiteData' => 'NetglueDefaults\Service\SiteDataFactory',
    ),

    'aliases' => array(
        'SiteData' => 'NetglueDefaults\Service\SiteData',

        'default-nav' => 'Zend\Navigation\Service\DefaultNavigationFactory',
    ),

);
